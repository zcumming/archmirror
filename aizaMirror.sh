#!/bin/bash

# Aizalabs Mirror Setup script
# Installs and configures required packages 
# Configures config boot files to output HDMI 1280*800@60Hz and quiet bootloader sequence
#   Author: Zach Cumming
#   Date: 6/16/16

#Colors and format for fancy output
bold=$(tput bold)
normal=$(tput sgr0)
green=$(tput setaf 2)
red=$(tput setaf 1)
yellow=$(tput setaf 3)

VERSION="0.1"

DNSMASQ_CONF='/etc/dnsmasq.conf'
HOSTAPD_CONF='/etc/hostapd/hostapd.conf'
XINIT_CONF='/etc/X11/xinit/xinitrc'
WI_PROFILE='/etc/netctl/wiMirror'
WPA_SUPPLICANT='/etc/wpa_supplicant/wpa_supplicant-wlan0.conf'
VIFACE_NAME=uap0
VIFACE_IP=192.168.3.1/24
DEFAULT_CHANNEL=3

#Package list to install with pacman
pkg_list=( hostapd dnsmasq apache php-apache sudo xorg-server xorg-server-utils xorg-xinit xf86-video-fbdev matchbox-window-manager midori xterm dosfstools wget tmux epiphany )

usage(){
    printf "${bold}AizaMirror Version: $VERSION\n${normal}"
    printf "Usage: ./aizaMirror.sh [OPTIONS] <start|stop|install|status>\n"
    printf "\n"
    printf "  start\t\t\t\t\tStart access point\n"
    printf "  stop\t\t\t\t\tStop access point\n"
    printf "  install\t\t\\t\tInstall script, config files, and required pkgs (requires internet)\n"
    printf "  status\t\t\t\tShow JSON formatted device status information\n"
    printf "  -h, --help\t\t\t\tDisplay this usage info\n"
    printf "  -b, --boot\t\t\t\tToggle boot at startup\n"
    printf "  -R, --reset\t\t\t\tStop all processes and reset all configuration files\n"
    printf "  -s, --scan\t\t\t\tScan for available wireless networks\n"
    printf "  -i, --inet\t\t\t\tTest internet connection\n"
    printf "  -a, --ipaddr\t\t\t\tRetrieve IP address of wlan0\n"
    printf "  -p, --password\t\t\tShow wireless hostapd password\n"
    printf "  -d, --display\t\t\t\tStart web based display\n"
    printf "  -c, --changeWifi\t\t\t\tRestart wifi connection & hostapd\n"
    printf "  -w, --wifi '<SSID>' '<PSK>' <WPA|WEP>\tSetup wifi networking\n"
    printf "        \t\t\t\tEx. aizaMirror -w 'yourSSID' 'yourPreSharedKey' WPA\n"
    printf "\n"
}

parse_parameters(){
    #Check user is root
    if [[ $EUID -ne 0 ]]; then
        echo "Please run as root"
        exit 1
    fi

    #Check if kernal version is Arch
    uname -r | grep ARCH > /dev/null
    if [[ $? -ne 0 ]]; then
        echo "Kernel release must be ARCH based\n"
        exit 1
    fi

    if [[ $# -eq 0 ]]; then
        usage
        exit 0
    fi

    #Parse flags
    while [[ $# -gt 0 ]]
    do
    key="$1"

    case $key in
        start)
        start
        ;;
        stop)
        stop
        ;;
        install)
        install_required_pkgs
        copy_confs
        init_script
        exit 0
        ;;
        status)
        status
        exit 0
        ;;
        -h|--help)
        usage
        exit 0
        ;;
        -b|--boot)
        toggle_boot_startup
        exit 0
        ;;
        -R|--reset)
        stop
        copy_confs
        exit 0
        ;;
        -w|--wifi)
        write_wi_profile "$@"
        sync
        shift 3
        ;;
        -s|--scan)
        scan_wireless
        exit 0
        ;;
        -i|--inet)
        network_ctrl test
        exit $?
        ;;
        -a|--ipaddr)
        show_ip
        exit 0
        ;;
        -c|--changeWifi)
        viface_init
        route_ip
        network_ctrl start
        start_hostapd
        ;;
        -p|--password)
        show_wireless_pw
        exit 0
        ;;
        -d|--display)
        start_display
        exit 0
        ;;
        *)
        printf "Unknown option: $key\n"
        usage
        exit 1
        ;;
    esac
    shift #past argument or value
    done
}

install_required_pkgs(){
    printf "Checking for installed packages..."

    #If any package isn't installed, synchronize database
    for pkg in "${pkg_list[@]}"
    do
        pacman -Qi $pkg > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            printf "${yellow}WARN${normal}\n"
            printf "<!> Package $pkg not found, attempting network install\n"
            pacman -Syy
            if [ $? -ne 0 ]; then
                printf "${red}FAIL${normal}\n"
                exit 1
            fi
            break
        fi
    done
 
    for pkg in "${pkg_list[@]}"
    do
        pacman -Qi $pkg > /dev/null 2>&1
        if [ $? -ne 0 ]; then
            printf "Installing package $pkg...\n"
            pacman -S --noconfirm $pkg 
            if [ $? -ne 0 ]; then
                printf "${red}FAIL${normal}\n"
                printf "<!> Error installing $pkg\n"
                exit 1
            fi
        fi
    done
  
    printf "${green}OK${normal}\n"
    return 0
}

copy_confs(){
    #Repsonsible to copying default configs
    conf_list=( aizaMirror.sh ./confs/dnsmasq.conf ./confs/hostapd.conf ./confs/cmdline.txt ./confs/config.txt ./confs/wiMirror ./confs/httpd.conf ./confs/index.php ./confs/aizaMirror.service )
    
    printf "Initializing default configuration files..."
    for conf in "${conf_list[@]}"
    do  
        if [ ! -f $conf ]; then
            printf "${red}FAIL${normal}\n"
            printf "<!> Default configuration file $conf not found\n"
            exit 1
        fi
    done

    #Copy aizaMirror.service
    cp ./confs/aizaMirror.service /etc/systemd/system/aizaMirror.service

    #Copy dnsmasq & hostapd
    cp ./confs/dnsmasq.conf $DNSMASQ_CONF
    cp ./confs/hostapd.conf $HOSTAPD_CONF

    #Copy boot.txt and cmdline.txt
    cp ./confs/config.txt /boot/config.txt
    cp ./confs/cmdline.txt /boot/cmdline.txt
 
    #Copy apache server PHP enabled config
    cp ./confs/httpd.conf /etc/httpd/conf/httpd.conf

    #Copy PHP pages page 
    cp ./confs/index.php /srv/http/index.php
    cp ./confs/joinAP.php /srv/http/joinAP.php

    #Copy sudoers file
    cp ./confs/sudoers /etc/sudoers

    #Auto login and auto startx
    cp ./confs/xinitrc /etc/X11/xinit/xinitrc
    cp ./confs/Xwrapper.config /etc/X11/Xwrapper.config
    if [ ! -d "/etc/systemd/system/getty@tty1.service.d/" ]; then
        mkdir /etc/systemd/system/getty@tty1.service.d/ > /dev/null
    fi
    cp ./confs/override.conf /etc/systemd/system/getty@tty1.service.d/override.conf
    systemctl reenable getty@tty1.service 2> /dev/null

    #Networking - eth0 and wlan0
    cp ./confs/eth0.network /etc/systemd/network/eth0.network
    cp ./confs/wlan0.network /etc/systemd/network/wlan0.network
    cp ./confs/wpa_supplicant@wlan0.service /etc/systemd/system/wpa_supplicant@wlan0.service
    systemctl reenable systemd-networkd.service 2> /dev/null
    systemctl reenable wpa_supplicant@wlan0.service 2> /dev/null

    #Script to run at login (shows temperature & ip address when logging in)
    cp ./confs/bash.bashrc /etc/bash.bashrc

    #Initialize i2c
    #append dtparam=i2c_arm=on to /boot/config.txt
    cp ./confs/raspberrypi.conf /etc/modules-load.d/raspberrypi.conf

    #Permit root login
    cp ./confs/ssh_config /etc/ssh/ssh_config

    #Journald.conf to set maximum journal size
    cp ./confs/journald.conf /etc/systemd/journald.conf

    printf "${green}OK${normal}\n"
    return 0
}

init_script(){
    #Responsible for boot auto startups and copying aizaMirror into /usr/local/bin

    #Copy aizaMirror.sh to /usr/local/bin
    printf "Copying script to /usr/local/bin/aizaMirror..."
    if [ -f aizaMirror.sh ]; then
        cp aizaMirror.sh /usr/local/bin/aizaMirror
    else
        printf "${red}FAIL${normal}\n"
        printf "! No file aizaMirror.sh found in current directory or /root/archmirror\n"
        exit 1
    fi
    printf "${green}OK${normal}\n"
  
    #Enable aizaMirror.service at startup through systemd
    printf "Enabling aizaMirror.service startup at boot..."
    systemctl enable aizaMirror.service 2> /dev/null
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        exit 1    
    else
        printf "${green}OK${normal}\n"
    fi

    #Enable httpd.service at startup through systemd
    printf "Enabling httpd.service startup at boot..."
    systemctl enable httpd.service 2> /dev/null
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        return 1
    else
        printf "${green}OK${normal}\n"
    fi
   
    #Set locale for tmux
    #uncomment en_US.UTF-8 in /etc/locale.gen
    #run locale-gen

    return 0
}

start_dnsmasq(){
    # arg $1 might be stop

    #Check for already running dnsmasq
    PID=$(pgrep dnsmasq)
    if [ ! -z $PID ]; then
        printf "Killing current dnsmasq PID $PID..."
        killall dnsmasq > /dev/null
        if [ $? -eq 0 ]; then
            printf "${green}OK${normal}\n"
        else
            printf "${red}FAIL${normal}\n"
        fi    
    fi
    
    if [[ $1 == "stop" ]]; then
        return 0
    fi

    #Starting dnsmasq
    if [ ! -f $DNSMASQ_CONF ]; then
        printf "Config file $DNSMASQ_CONF not found\n"
    fi

    printf "Starting dnsmasq with conf file $DNSMASQ_CONF..."
    #/usr/bin/dnsmasq -C $DNSMASQ_CONF 2> /dev/null
    /usr/bin/dnsmasq -q -C $DNSMASQ_CONF
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        printf "! Failed to start dnsmasq with conf file $DNSMASQ_CONF\n"
        exit 1
    fi
    printf "${green}OK${normal}\n"

}

start_hostapd(){
    #Check for already running dnsmasq
    PID=$(pgrep hostapd)
    if [ ! -z $PID ]; then
        printf "Killing current hostapd PID $PID..."
        killall hostapd > /dev/null
        if [ $? -eq 0 ]; then
            printf "${green}OK${normal}\n"
        else
            printf "${red}FAIL${normal}\n"
        fi    
    fi
    
    if [[ $1 == "stop" ]]; then
        return 0
    fi
 
    #Starting hostapd
    if [ ! -f $HOSTAPD_CONF ]; then
        printf "Config file $HOSTAPD_CONF not found\n"
        return 1
    fi

    #Generate random password for AP
    RANDOM_PW=$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-8};echo)
    sed -i '/wpa_passphrase/d' $HOSTAPD_CONF
    printf "wpa_passphrase=$RANDOM_PW\n" >> $HOSTAPD_CONF
    
    #Parse channel if router authenticated
    CHANNEL=$(iwlist wlan0 channel | grep 'Current' | awk -F 'Channel ' '{print $2}' | sed 's/.$//')
    if [ -z $CHANNEL ]; then
        CHANNEL=$DEFAULT_CHANNEL
    fi
    sed -i '/channel/d' $HOSTAPD_CONF
    printf "channel=$CHANNEL\n" >> $HOSTAPD_CONF

    printf "Starting hostapd with password ${bold}$RANDOM_PW${normal} on channel $CHANNEL..."
    /usr/bin/hostapd -B $HOSTAPD_CONF > /dev/null
    #/usr/bin/hostapd -B $HOSTAPD_CONF -dd
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        printf "<!> Failed to start hostapd with conf file $HOSTAPD_CONF\n"
        return 1
    fi
    printf "${green}OK${normal}\n"
    return 0
}

reset_confs(){
    #Resets configs and removed $WI_PROFILE

    copy_confs

    printf "Removing wlan0 inet profile $WI_PROFILE..."
    if [ -f $WI_PROFILE ]; then
        rm $WI_PROFILE 2> /dev/null
        printf "${green}OK${normal}\n"
    fi
}

reload_brcmfmac(){
    printf "Reloading kernel module brcmfmac..."
    modprobe -r brcmfmac
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        printf "! Failed to remove brcmfmac\n"
        return 1
    fi
    modprobe brcmfmac
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        printf "! Failed to load brcmfmac\n"
        return 1
    fi

    #After kernel module reload, wlan0 takes ~1 sec to come up
    TIMER=0
    until iw wlan0 info > /dev/null || [ $TIMER -eq 10 ]; do
        #printf "$TIMER..."
        sleep 1
        TIMER=$(( TIMER++ ))
    done

    if [ "$TIMER" -gt 9 ]; then
        printf "${red}FAIL${normal}\n"
        printf "wlan0 bringup timeout\n"
        exit 1
    fi

    printf "${green}OK${normal}\n"
    return 0
}

viface_init(){
    iw $VIFACE_NAME info > /dev/null
    if [ $? -eq 0 ]; then
        reload_brcmfmac
    fi

    printf "Configuring virtual interface $VIFACE_NAME at inet $VIFACE_IP..."
    iw dev wlan0 interface add $VIFACE_NAME type __ap
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        reload_brcmfmac
    fi

    ip addr add $VIFACE_IP dev $VIFACE_NAME
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        printf "! Assigning static IP $VIFACE_IP to interface $VIFACE_NAME failed\n"
        return 1
    fi

    ip link set $VIFACE_NAME up 2> /dev/null
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        printf "! Failed to bring up virtual interface $VIFACE_NAME\n"
        return 1
    fi
    
    printf "${green}OK${normal}\n"
    return 0
}

route_ip(){
    printf "Configuring iptables and ipv4 forwarding..."
    sysctl net.ipv4.ip_forward=1 > /dev/null
    iptables -t nat -F
    iptables -t nat -A POSTROUTING -s 192.168.3.0/24 ! -d 192.168.3.0/24 -j MASQUERADE
    if [ $? -ne 0 ];
    then
        printf "${red}FAIL${normal}\n"
        printf "! Failed to setup ip routing\n"
        exit 1
    fi

    printf "${green}OK${normal}\n"
    return 0
}

scan_wireless(){
    OPER_STATE=$(cat /sys/class/net/wlan0/operstate)

    if [ $OPER_STATE != "up" ]
    then
        ip link set wlan0 up
    fi

    json_APs="["

    scanResults=$(iwlist wlan0 scan 2> /dev/null | grep 'Cell\|ESSID\|IEEE 802.11\|Quality\|Channel' | tr -d '\n')

    i=2
    eachAP="init"
    while [ -n "$eachAP" ]
    do
        eachAP=$( awk -F 'Cell ' '{print $'$i'}' <<< "$scanResults" )
        ssid_security=$( awk -F 'ESSID:' '{print $2}' <<< "$eachAP" )
        if [[ $ssid_security != *"\x00"* ]] && [ -n "$ssid_security" ]; then
            ssid=$( awk -F 'IE:' '{print $1}' <<< "$ssid_security" | sed 's/.*"\(.*\)".*/\1/g' )
            json_APs+="{\"SSID\":\"$ssid\","

            security=$( awk -F 'IE: ' '{print $2}' <<< "$ssid_security" )
            json_APs+="\"Security\":"
            if [[ "$security" == *"WEP"* ]]; then
                json_APs+="\"WEP\","
            elif [[ "$security" == *"WPA"* ]]; then
                json_APs+="\"WPA\","
            else
                json_APs+="\"OPEN\","
            fi

            quality=$( awk -F "Quality=" '{print $2}' <<< "$eachAP" | cut -f1 -d" " )
            json_APs+="\"Quality\":\"$quality\","
                 
            channel=$( awk -F "Channel:" '{print $2}' <<< "$eachAP" | cut -f1 -d" " )
            json_APs+="\"Channel\":\"$channel\"},"
        fi
        i=$(( $i + 1 ))
    done
    
    json_APs_formatted=${json_APs::-1}
    json_APs_formatted+="]"   
 
    printf "$json_APs_formatted\n"
}

toggle_boot_startup(){
   if [ -f /etc/systemd/system/multi-user.target.wants/aizaMirror.service ]; then
        #Disable 
        printf "Disabling auto startup at boot..."
        systemctl disable aizaMirror.service 2> /dev/null
        if [ $? -ne 0 ]; then
            printf "${red}FAIL${normal}\n"
            exit 1
        fi
    else
        #Enable
        printf "Enabling auto startup at boot..."
        systemctl enable aizaMirror.service 2> /dev/null
        if [ $? -ne 0 ];then
            printf "${red}FAIL${normal}\n"
            exit 1    
        fi
    fi
    
    printf "${green}OK${normal}\n"
    return 0
}

write_wi_profile(){
    #Args: $2 = SSID, $3 = PASSPHRASE, $4 = Security (WPA/WEP)
    printf "Writing wireless profile and wpa_supplicant..."
    
    if [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]; then
        printf "${red}FAIL${normal}\n"
        printf "<!> Incorrect format or missing arguments\n"
        printf "<?> Check usage\n"
        exit 1
    fi
   
    #Write wpa_supplicant file
    printf "ctrl_interface=/run/wpa_supplicant\n" > $WPA_SUPPLICANT 
    printf "update_config=1\n" >> $WPA_SUPPLICANT
    printf "country=US\n" >> $WPA_SUPPLICANT
    printf "p2p_disabled=1\n" >> $WPA_SUPPLICANT
    printf "\n" >> $WPA_SUPPLICANT
    
    if [[ $4 == "WPA" ]] || [[ $4 == "wpa" ]]; then
        printf "#Simple WPA1 or WPA2\n" >> $WPA_SUPPLICANT
        printf "network={\n" >> $WPA_SUPPLICANT
        printf "\tssid=\"$2\"\n" >> $WPA_SUPPLICANT
        printf "\tpsk=\"$3\"\n" >> $WPA_SUPPLICANT
        printf "\tkey_mgmt=WPA-EAP WPA-PSK IEEE8021X NONE\n" >> $WPA_SUPPLICANT
        printf "\tgroup=WEP40 WEP104 TKIP CCMP\n" >> $WPA_SUPPLICANT
        printf "}\n\n" >> $WPA_SUPPLICANT
        printf "${green}OK${normal}\n"
        return 0
    fi
   
    if [[ $4 == "WEP" ]] || [[ $4 == "wep" ]]; then 
        printf "#SHARED WEP\n" >> $WPA_SUPPLICANT
        printf "network={\n" >> $WPA_SUPPLICANT
        printf "\tssid=\"$2\"\n" >> $WPA_SUPPLICANT
        printf "\tkey_mgmt=NONE\n" >> $WPA_SUPPLICANT
        printf "\tauth_alg=SHARED\n" >> $WPA_SUPPLICANT
        printf "\tgroup=WEP40 WEP104 TKIP CCMP\n" >> $WPA_SUPPLICANT
        printf "\teapol_flags=3\n" >> $WPA_SUPPLICANT
        printf "\twep_key0=\"$3\"\n" >> $WPA_SUPPLICANT 
        printf "\twep_key1=\"$3\"\n" >> $WPA_SUPPLICANT 
        printf "\twep_key2=\"$3\"\n" >> $WPA_SUPPLICANT 
        printf "\twep_key3=\"$3\"\n" >> $WPA_SUPPLICANT 
        printf "\twep_tx_keyidx=0\n" >> $WPA_SUPPLICANT
        printf "}\n\n" >> $WPA_SUPPLICANT

        printf "#OPEN WEP\n" >> $WPA_SUPPLICANT
        printf "network={\n" >> $WPA_SUPPLICANT
        printf "\tssid=\"$2\"\n" >> $WPA_SUPPLICANT
        printf "\tkey_mgmt=WPA-EAP WPA-PSK IEEE8021X NONE\n" >> $WPA_SUPPLICANT
        printf "\tgroup=WEP40 WEP104 TKIP CCMP\n" >> $WPA_SUPPLICANT
        printf "\twep_key0=\"$3\"\n" >> $WPA_SUPPLICANT
        printf "\twep_key1=\"$3\"\n" >> $WPA_SUPPLICANT
        printf "\twep_key2=\"$3\"\n" >> $WPA_SUPPLICANT
        printf "\twep_key3=\"$3\"\n" >> $WPA_SUPPLICANT
        printf "\twep_tx_keyidx=0\n" >> $WPA_SUPPLICANT
        printf "}\n\n" >> $WPA_SUPPLICANT
        printf "${green}OK${normal}\n"
        return 0
    fi

    printf "${red}FAIL${normal}\n"
    printf "<!> Unknown security: $4\n"
    return 1
}

network_ctrl(){
    if [[ $1 == "start" ]]; then
        printf "Starting wpa_supplicant@wlan0.service..." 
        systemctl restart wpa_supplicant@wlan0.service 2>/dev/null
        if [ $? -ne 0 ]; then
            printf "${yellow}WARN${normal}\n"
            printf "<!> Failed to start wpa_supplicant@wlan0.service\n"
            return 1
        fi
        sleep 2 #Required to allow time for iwconfig to fill in
    elif [[ $1 == "stop" ]]; then
        printf "Stopping wpa_supplicant@wlan0.service..."
        systemctl stop wpa_supplicant@wlan0.service 2> /dev/null   
        if [ $? -ne 0 ]; then
            printf "${yellow}WARN${normal}\n"
            printf "<!> Failed to stop wpa_supplicant@wlan0.service\n"
            return 1
        fi
    elif [[ $1 == "test" ]]; then
        printf "Checking router connection..."
        if [ "$(iwconfig wlan0 | grep ESSID:off)" ]; then
            printf "${red}FAIL${normal}\n"
            return 1
        else
            printf "${green}OK${normal}\n"
        fi

        printf "Checking internet connection..."
        #Check interface state
        OPER_STATE=$(cat /sys/class/net/wlan0/operstate)
    
        if [ $OPER_STATE != "up" ]
        then
            printf "${red}FAIL${normal}\n"
            printf "<!> Wireless interface wlan0 down\n"
            return 1
        fi

        ping_list=( www.google.com www.yahoo.com www.facebook.com www.stackoverflow.com )

        for site in "${ping_list[@]}"
        do
            ping -q -w 1 -c 1 $site &> /dev/null
            if [ $? -eq 0 ]; then
                printf "${green}OK${normal}\n"
                return 0
            fi
        done
    
        printf "${red}FAIL${normal}\n"
        printf "<!> No internet connection detected\n"
        return 1
    else
        printf "${red}FAIL${normal}\n"
        printf "<!> Unknown option: $1\n"
        return 1
    fi
    
    printf "${green}OK${normal}\n"
    
    return 0
}

start_display(){
    JOINAP='http://localhost/joinAP.php'
    AIZALABS='http://www.aizalabs.com/mirror/private_ildo.php'
    
    #Need to check ping of localhost instead of active service
    #Check httpd.serivce is active
    if [ "$(systemctl is-active httpd.service)" != "active" ]; then
        printf "<!> Restarting httpd.service\n"
        systemctl restart httpd.service
    fi

    #Removing midori command from xinit
    sed -i '/midori/d' $XINIT_CONF
    sed -i '/epiphany/d' $XINIT_CONF

    #Check inet connection and/or router connection
    network_ctrl test
    if [ $? -eq 0 ]; then
        #printf "epiphany $JOINAP\n" >> $XINIT_CONF
        printf "midori -e Fullscreen -a $AIZALABS &>/dev/null\n" >> $XINIT_CONF
    else
        #Check here for router authentication
        #If router not connected, go to JOINAP
        #If router is connected, go to INETDOWN
        printf "midori -e Fullscreen -a $JOINAP &>/dev/null\n" >> $XINIT_CONF
        #printf "epiphany $JOINAP\n" >> $XINIT_CONF
    fi

    #Check if X server is already running, if so restart
    if [ ! -z $(pgrep Xorg) ]; then
        printf "Killing current X server..."
        pkill -15 Xorg
        if [ $? -ne 0 ]; then
            printf "${red}FAIL${normal}\n"
        else    
            printf "${green}OK${normal}\n"
        fi
            
        sleep 1
    fi

    printf "Starting X server..."
    [ -z "$DISPLAY" -a "$(fgconsole)" -eq 1 ] && su -c 'xinit /etc/X11/xinit/xinitrc 2>/dev/null &' alarm
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        return 1
    else    
        printf "${green}OK${normal}\n"
    fi
    return 0
}

status(){
    #JSON format must not have trailing ',' after last element
    json_status="{"    
    
    #CPU temperature
    cpu_temp=$(/opt/vc/bin/vcgencmd measure_temp | awk -F '=' '{print $2}')
    json_status+="\"cpu_temp\":\"$cpu_temp\","

    #Hostapd status
    json_status+="\"hostapd\":"
    if [ ! -z $(pgrep hostapd) ]; then
        json_status+="\"ACTIVE\","
    else
        json_status+="\"DEAD\","
    fi
    
    #Hostapd password
    hostapd_pw=$(cat $HOSTAPD_CONF | grep wpa_passphrase | awk -F'=' '{print $2}')
    json_status+="\"ap_pswrd\":\"$hostapd_pw\","

    #Hostapd channel
    hostapd_chan=$(cat $HOSTAPD_CONF | grep channel | awk -F'=' '{print $2}')
    json_status+="\"ap_chan\":\"$hostapd_chan\","
 
    #dnsmasq status
    json_status+="\"dnsmasq\":"
    if [ ! -z $(pgrep dnsmasq) ]; then
        json_status+="\"ACTIVE\","
    else
        json_status+="\"DEAD\","
    fi

    #Virtual interface configured
    json_status+="\"viface\":"
    if [ ! -z "$(ip address show dev uap0 2>/dev/null | grep inet)" ]; then
        json_status+="\"$VIFACE_IP\","
    else
        json_status+="\"DEAD\","
    fi

    #Status for authentication with Access Point
    json_status+="\"router_auth\":"
    if [ "$(iwconfig wlan0 | grep ESSID:off)" ]; then
        json_status+="\"No connection\","
    else
        ESSID=$(iwconfig wlan0 | grep ESSID | awk -F ':' '{print $2}')
        json_status+="\"Connected\","
    fi

    #Internet connectivity and IPV4 address
    IP_ADDR="$(ip addr show wlan0 | grep "inet " | awk '{print $2}' | sed 's/[/].*$//')"
    if [ -z $IP_ADDR ]; then
        json_status+="\"wlan0_ipv4\":\"No connection\","
    else
        json_status+="\"wlan0_ipv4\":\"$IP_ADDR\","
    fi

    #Removing trailing comma
    json_status_formatted=${json_status::-1}
    json_status_formatted+="}\n"

    printf "$json_status_formatted"
}

start() {
    #This order of initialization is critical, do not change
    viface_init
    route_ip
    start_dnsmasq
    network_ctrl start
    start_display
    start_hostapd
    exit 0
}

stop() {
    #Reset iptables and ip forwarding
    printf "Resetting iptables and ipv4 forwarding..."
    sysctl net.ipv4.ip_forward=0 > /dev/null
    iptables -t nat -F
    if [ $? -ne 0 ]; then
        printf "${red}FAIL${normal}\n"
        exit 1
    else
        printf "${green}OK${normal}\n"
    fi
    
    start_dnsmasq stop
    start_hostapd stop    
  
    network_ctrl stop
 
    #Stop X sever
    #printf "Stopping X server..."
    #pkill -15 Xorg 2> /dev/null
    #if [ $? -ne 0 ]; then
    #    printf "${red}FAIL${normal}\n"
    #    printf "! Failed to stop X server\n"
    #else
    #    printf "${green}OK${normal}\n"
    #fi
 
    #Stop apache server (httpd)
    #printf "Stopping httpd service..."
    #systemctl stop httpd.service 2> /dev/null
    #if [ $? -ne 0 ]; then
    #    printf "${red}FAIL${normal}\n"
    #    printf "! Failed to stop httpd\n"
    #    exit 1
    #else
    #    printf "${green}OK${normal}\n"
    #fi

    #Reload kernel module for wireless driver
    reload_brcmfmac
}

#Main execution
parse_parameters "$@"
