#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '
PS2='> '
PS3='> '
PS4='+ '

case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

    ;;
  screen)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
esac

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion

cpuTemp=$(/opt/vc/bin/vcgencmd measure_temp | awk -F '=' '{print $2}')
echo "CPU Temp: $cpuTemp"

ipAddr="$(ip addr show wlan0 | grep "inet " | awk '{print $2}' | sed 's/[/].*$//')"
if [ -z $ipAddr ]; then
    echo "wlan0 IPv4: No connection"
else
    echo "wlan0 IPv4: $ipAddr"
fi

hostapd_pw=$(cat /etc/hostapd/hostapd.conf | grep wpa_passphrase | awk -F'=' '{print $2}')
echo "hostapd_pw: $hostapd_pw"
