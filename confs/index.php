<?php
    $json_status = exec("sudo aizaMirror status");
    $aizaStatus=json_decode($json_status, true);
   
    session_start();
  
    echo "IP Address: {$aizaStatus['wlan0_ipv4']}<br/><br/>";

    if(!isset($_SESSION['SCAN_EXEC'])){
        $json_scanResults = exec("sudo aizaMirror --scan");
        $ssid_array=json_decode($json_scanResults, true);
        $_SESSION['SCAN_EXEC'] = 1;
    }
    
    if(count($_POST) > 0){
        $_SESSION['ssid_key'] = $_POST['ssid_key'];
        $_SESSION['postDump'] = $_POST;
        $_SESSION['ssid_list'] = $_POST['ssid_list'];

        header("HTTP/1.1 303 See Other");
        header("Location: http://$_SERVER[HTTP_HOST]/index.php");
        die();
    }
    else if (isset($_SESSION['ssid_key'])){
        $ssid_key = $_SESSION['ssid_key'];
        $postDump = $_SESSION['postDump'];    
        $ssid_list = $_SESSION['ssid_list'];
 
        $ssidAndSecurity=explode(":",$ssid_list);
        $ssid = $ssidAndSecurity[0];
        $security = $ssidAndSecurity[1]; 

        if (isset($ssid) && $ssid_key!="" && isset($security)){
            exec("sudo aizaMirror -w '$ssidAndSecurity[0]' '$ssid_key' '$ssidAndSecurity[1]'", $output, $return);
            if ($return=="0"){
                echo "Starting networking...";
                exec("sudo aizaMirror -c", $startNetworkOutput, $startNetworkReturn);
                echo $startNetworkReturn;
            }
        }
        else if ($ssid_key == ""){
            $status="Error: Please enter a wireless password";
        }
 
        session_unset();
        session_destroy();
    }

    //Unsetting SCAN_EXEC so the wireless scan runs at page refresh
    unset($_SESSION['SCAN_EXEC']);

?>
<html>
<head>
    <title>Wireless Setup</title>
    <style type="text/css">
        body {
            font-size: 20pt;
            text-align: left;
            font: Verdana, Arial, sans-serif;
        }
        p {
            color: black;
            font-size: 20pt;
            font: Noto Mono;
        }
    </style>
</head>
<body>
    <p>AizaMirror Configuration Utility</p> 

    <form method="POST">
        <label for="ssid_list">Select Wifi Network: </label><br/>
        <select id='ssid_list' name="ssid_list">
        <?php
            foreach($ssid_array as $key => $array){
                $allKeys = array_keys($array);
                $allValues = array_values($array);
                echo "<option value='$allValues[0]:$allValues[1]'>$allValues[0] - Security: $allValues[1] - Signal: $allValues[2]</option>\n";
            }
        ?>
        </select>
        <br><br>
        <label for="ssid_key">Wifi Password: </label><br/>
        <input type='password' name='ssid_key' />
        <input type="submit">
        <?php echo "<p>$status</p>"?>
        <?php print_r($postDump)?>
    </form>
</body>
</html>
