<?php
    $page = $_SERVER['PHP_SELF'];
    $refreshSec = "30";

    $json_status = exec("sudo aizaMirror status");
    $status=json_decode($json_status, true);
    $vifaceIP=explode("/",$status['viface']);

    $router_auth=$status['router_auth'];
    $wlan0_ipv4=$status['wlan0_ipv4'];

    function inet_check(){
        if ($GLOBALS['wlan0_ipv4'] != 'No connection'){
            header("Location: http://www.aizalabs.com/mirror/private_ildo.php");
        }
    }

    inet_check();

?>
<html>
<head>
    <meta http-equiv="refresh" content="<?php echo $refreshSec?>;URL='<?php echo $page?>'">
    <title>AizaMirror JoinAP</title>
    <style type="text/css">
        html {
            background-color: #000000;
        }

        body {
            color: white;
            font-size: 36pt;
            text-align: center;
        }
        
        p {
            color: white;
            font-size: 14pt;
        }
    </style>
</head>
<body>
    <br/><br/><br/><br/><br/>
    Welcome.
    <br/><br/><br/><br/>
    Wireless SSID: AizaMirror
    <br/>
    Password: 
    <?php
        echo "{$status['ap_pswrd']}";
    ?>
    <br/><br/><br/><br/><br/><br/>
    <p>To configure your AizaMirror, join the wireless hotspot "AizaMirror" from your smartphone or computer and navigate to <?php echo $vifaceIP[0]?> in your web browser.</p>
    
</body>
</html>
